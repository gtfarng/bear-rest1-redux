import React, { Component } from 'react'
import { connect } from "react-redux"
import './App.css'
import { getBears,addbear, store } from "./App"


class crudBear extends Component {
    componentDidMount = () => {
        this.props.getBears()
    }

    state = { bearState: '' }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    // addbear = () => {
    //     this.handleChange()
    //     axios.post(`http://localhost/api/bears`)
    //     this.componentDidMount()
    //     console.log('SS'+this.state.bearState.bearname);

    // }
    // delbear = () => {
    //     axios.delete(`http://localhost/api/bears/`)
    // }


    render() {
        return (
            <div align="center">
                <h1>CRUD Bear</h1><br/>
                <div>
                    <div>
                        <input name="id" value={this.props.id} placeholder='Enter ID to Delete' />
                    </div>
<br/>

                    <div>
                        
                        <input type='text' name="bearname" placeholder='Name...' value={this.props.bearname} onChange={this.handleChange} />
                    </div>
<br/>
                    <div>
                        
                        <input type='number' name="weight" placeholder='weight...' value={this.props.weight} onChange={this.handleChange} /><br />
                    </div>
                </div>
<br/><br/>
                &nbsp;&nbsp;<button className="btn btn-outline-primary" onClick={() => store.dispatch(addbear(this.state.bearname, this.state.weight))}>ADD</button>
               &nbsp;&nbsp; <button className="btn btn-outline-success" >GET</button>
               &nbsp;&nbsp; <button className="btn btn-outline-warning" >UPDATE</button>
                &nbsp;&nbsp;<button className="btn btn-outline-danger"  >DELETE</button>
                <br/><br/><br/>
            </div>
        )
    }
}

const mapStateToProps = ({ bears }) => { return { bears } }

const mapDispatchToProps = (dispatch) => {
    return {
        addbear: () => dispatch(addbear()),
        getBears: () => store.dispatch(getBears()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(crudBear)