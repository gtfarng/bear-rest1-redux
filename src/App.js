import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import axios from "axios"
import thunk from 'redux-thunk'
import logo from './panda.svg'

import Bear from "./Bear"
import CRUDBear from './crudBear'


//Get
export const getBearsSuccess = bears => ({
  type: 'GET_BEARS_SUCCESS',
  bears
});
export const getBearsFailed = () => ({ type: 'GET_BEARS_FAILED' });

export const getBears = () => async (dispatch) => {
  try {
    console.log('Get Bear New')
    const response = await axios.get(`http://gtfarng-restful.ddns.net:8003/api/bears`)
    const responseBody = await response.data;
    console.log('response: ', responseBody)
    dispatch(getBearsSuccess(responseBody));
  } catch (error) {
    console.error(error);
    dispatch(getBearsFailed());
  }
}

//ADD
export const addbear = (bearname, weight) => async (dispatch) => {
  try {
    console.log('Add Bear New')
    if (bearname !== undefined && weight !== undefined) {
      await axios.post(`http://gtfarng-restful.ddns.net:8003/api/bears`, { name: bearname, weight: weight })
      const response = await axios.get(`http://gtfarng-restful.ddns.net:8003/api/bears`)
      const responseBody = await response.data;
      console.log('response: ', responseBody)
      dispatch(getBearsSuccess(responseBody))
    }
  } catch (error) {
    console.error(error);
    dispatch(getBearsFailed());
  }
}


export const bearReducer = (state = 0, action) => {
  switch (action.type) {
    case 'GET_BEARS_SUCCESS':
      console.log('action: ', action.bears)
      return action.bears
    case 'GET_BEARS_FAILED':
      console.log('action: Failed')
      return action.bears
    default:
      return state
  }
}

//Map Reducer
const rootReducer = combineReducers({  bears: bearReducer})

export const store = createStore(rootReducer, applyMiddleware(logger, thunk))

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
       <br/>
        <img src={logo} alt="logo" /> <br/>
        <CRUDBear />

        <Bear />
        <br/><br/><br/><br/><br/><br/>
      </Provider>
    );
  }
}

