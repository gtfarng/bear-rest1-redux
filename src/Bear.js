import React, {Component} from 'react'
//import axios from "axios"
import { store } from "./App"
import {connect} from "react-redux"
import {getBears} from "./App"

class Bear extends Component {

    // state = { bears: ''}

    // componentDidMount = () => {
    //     axios.get('http://localhost/api/bears')
    //         .then( (res) => {
    //             console.log(res.data)
    //             this.setState({bears: res.data })
    //         })
    //         .catch((err) => {
    //             console.log(err)
    //         })
    // }

    // componentDidMount = async() => {
    //     const bears = await axios.get('http://localhost/api/bears')
    //     console.log(bears)
    //     this.setState({bears: bears.data })
    // }

    componentDidMount = () => {
        this.props.getBears()
    }


    renderBear = () => {
        if ( this.props.bears )
            return  this.props.bears.map( (bear,index) =>
                (<ul>
                        <div class="card" align="center">
                         <div class="card-body" >


                            <h5 key={index}>{bear.id}. Name : <small>{bear.name}</small>, Weight : <small>{bear.weight}</small></h5> 


                         </div>
                        </div>



                   
                    </ul>)
            )
    }

    render() {
        return (
            <div>
                <ul>
                    { this.renderBear()}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = ( {bears} ) => { return {bears} }

const mapDispatchToProps = (dispatch) => {
    return {
        getBears:  () => store.dispatch(getBears()),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Bear);